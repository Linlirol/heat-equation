#include <iostream>
#include <vector>
#include <cmath>
#include "SLAE_lib.h"
using std::cout;
using std::cin;
using std::exp;
using std::endl;
using std::abs;
using std::size_t;
using std::sqrt;
typedef std::vector<double> vec;
using namespace SLAE_mini;


double k_fu(double x){
    return exp(sin(x));
}
double q_fu(double x) {
    return exp(cos(x));
}

double f_fu(double x) {
    return 1.;
}



int main() {
    int N = 0;
    cin >> N;
    double x0 = 0;
    double xN = 1;
    double h = (xN - x0) / (N-1);

    double k_ = k_fu((xN - x0) / 2);
    double q_ = q_fu((xN - x0) / 2);
    double f_ = f_fu((xN - x0) / 2);
    
    /// Массив значений всех функций:
    vec x(N);

    for (size_t i = 0; i < N; ++i) {
        x[i] = i * h;       
    }
    ////

    /// Аналитическое решение модельной задачи:

    double lambda1 = sqrt(q_ / k_);
    double lambda2 = -lambda1;
    double A = f_ / q_;
    double M1 = k_ * lambda1 - 1 + (k_ * lambda1 + 1) * exp(lambda1 * xN);
    double M2 = k_ * lambda2 - 1 + (k_ * lambda2 + 1) * exp(lambda2 * xN);
    double C1 = A / (  k_ * lambda1 - 1 - M1 / M2 * (k_ * lambda2 - 1)  );
    double C2 = A / (  -M2 / M1 * (k_ * lambda1 - 1) + k_ * lambda2 - 1  );

    vec u_man(N);
    for (size_t i = 0; i < N; ++i) {
        u_man[i] = C1 * exp(lambda1 * x[i]) + C2 * exp(lambda2 * x[i]) + A;
    }
    ///

    /// Решаем численно:
    // Заполняем трехдиагональную матрицу
    vec a_m(N-1);
    vec b_m(N);
    vec c_m(N-1);
    vec f_m(N);

    a_m[N-2] = - k_;
    b_m[0] = h + k_; b_m[N-1] = h + k_;
    c_m[0] = -k_;
    f_m[0] = 0;
    f_m[N-1] = 0;

    for (size_t i = 1; i < N-1; ++i) {
        b_m[i] = - 2 * k_ - q_ * h * h;
        c_m[i] = k_;
        a_m[i-1] = k_;

        f_m[i] = -f_ * h * h;
    }
    tridiag_matrix_dyn<double> A_m(N, a_m, b_m, c_m);

    /// Расчет модельного решения:
    vec u_mca = tridiag_solver(A_m, f_m);
    ///

    /// Считаем норму разностей для модельной задачи:
    double norm = abs(u_man[0] - u_mca[0]);
    for (size_t i = 1; i < N; ++i) {
        double tmp = abs(u_man[i] - u_mca[i]);
        if (norm < tmp)
            norm = tmp;
    }
    cout << N << ";" << norm << endl;
}
