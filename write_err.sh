#!/usr/bin/bash

mkdir -p build 
cd build

cmake ../
make

rm error.txt
for i in $(seq 101 100 655361)
do
    echo $i | ./p4v7_bench >> error.txt
done
mv error.txt ../scripts
cd ../scripts

python3 graph.py
cd ../
