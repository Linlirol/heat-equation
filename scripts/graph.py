import numpy as np
import matplotlib.pyplot as plt

file = "error.txt"

nums = []
errs = []

with open(file) as f:
    for line in f.readlines():
        num, err = [float(x) for x in line.split(';')]
        nums.append(num)
        errs.append(err)

fig, ax = plt.subplots()
ax.plot(nums, errs)
ax.set_xlabel("N")
ax.set_ylabel("err")
ax.semilogy()
ax.grid()
ax.minorticks_on()
fig.savefig('graph.png', dpi=300)
