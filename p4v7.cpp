#include <iostream>
#include <vector>
#include <cmath>
#include "SLAE_lib.h"
using namespace SLAE_mini;
using std::cout;
using std::exp;
using std::endl;
using std::abs;
using std::size_t;
using std::sqrt;
typedef std::vector<double> vec;


double k_fu(double x){
    return exp(sin(x));
}
double q_fu(double x) {
    return exp(cos(x));
}

double f_fu(double x) {
    return 1.;
}



int main() {
    int N = 655361;
    double x0 = 0;
    double xN = 1;
    double h = (xN - x0) / (N-1);

    double k_ = k_fu((xN - x0) / 2);
    double q_ = q_fu((xN - x0) / 2);
    double f_ = f_fu((xN - x0) / 2);
    cout << "Центр отрезка: " << (xN - x0) / 2 << endl;
    cout << "Количество шагов: " << N << ", шаг: " << h << "\n";

    /// Массив значений всех функций:
    vec x(N);

    for (size_t i = 0; i < N; ++i) {
        x[i] = i * h;       
    }
    ////

    /// Аналитическое решение модельной задачи:

    double lambda1 = sqrt(q_ / k_);
    double lambda2 = -lambda1;
    double A = f_ / q_;
    double M1 = k_ * lambda1 - 1 + (k_ * lambda1 + 1) * exp(lambda1 * xN);
    double M2 = k_ * lambda2 - 1 + (k_ * lambda2 + 1) * exp(lambda2 * xN);
    double C1 = A / (  k_ * lambda1 - 1 - M1 / M2 * (k_ * lambda2 - 1)  );
    double C2 = A / (  -M2 / M1 * (k_ * lambda1 - 1) + k_ * lambda2 - 1  );

    vec u_man(N);
    for (size_t i = 0; i < N; ++i) {
        u_man[i] = C1 * exp(lambda1 * x[i]) + C2 * exp(lambda2 * x[i]) + A;
    }
    ///

    /// Решаем численно:
    // Заполняем трехдиагональную матрицу
    vec a_m(N-1);
    vec b_m(N);
    vec c_m(N-1);
    vec f_m(N);

    a_m[N-2] = - k_;
    b_m[0] = h + k_; b_m[N-1] = h + k_;
    c_m[0] = -k_;
    f_m[0] = 0;
    f_m[N-1] = 0;

    for (size_t i = 1; i < N-1; ++i) {
        b_m[i] = - 2 * k_ - q_ * h * h;
        c_m[i] = k_;
        a_m[i-1] = k_;

        f_m[i] = -f_ * h * h;
    }
    tridiag_matrix_dyn<double> A_m(N, a_m, b_m, c_m);

    /// Расчет модельного решения:
    vec u_mca = tridiag_solver(A_m, f_m);
    ///

    /////////////////////// Сравнение аналитического и численного решений:
    cout << "------- РЕШЕНИЕ МОДЕЛЬНОЙ ЗАДАЧИ: ------\n";
    for (size_t i = 0; i < N; i += (N-1) / 10 ) {
        cout << "В точке x = " << x[i] << ": u_ан = " << u_man[i] << ", u_чис = " << u_mca[i];
        cout << "; |ui - [ui]| = " << abs(u_man[i] - u_mca[i]) <<  endl;
    }
    /// Считаем норму разностей для модельной задачи:
    double norm = abs(u_man[0] - u_mca[0]);
    for (size_t i = 1; i < N; ++i) {
        double tmp = abs(u_man[i] - u_mca[i]);
        if (norm < tmp)
            norm = tmp;
    }
    cout << "\nПОЛНАЯ ОШИБКА: ||ui - [ui]|| = " << norm << endl;
 
 
    cout << "\n\n";

    /// Численное решение полной задачи:
    vec a(N-1);
    vec b(N);
    vec c(N-1);
    vec f_c(N);

    a[N-2] = k_fu(x[N-1]);
    b[0] = h + k_fu(x[0]);
    b[N-1] = -h - k_fu(x[N-1]);
    c[0] = - k_fu(x[0]);
    f_c[0] = 0; f_c[N-1] = 0;

    for (size_t i = 1; i < N-1; ++i) {
        a[i-1] = k_fu(x[i] - 1/2 * h);
        b[i] = ( -k_fu(x[i] + 1/2 * h) - k_fu(x[i] - 1/2 * h) ) - q_fu(x[i]) * h * h;
        c[i] = k_fu(x[i] + 1/2 * h);
        f_c[i] = -f_fu(x[i]) * h * h;
    }
    tridiag_matrix_dyn<double> A_c(N, a, b, c);

    vec u_com = tridiag_solver(A_c, f_c);

    cout << "------- РЕШЕНИЕ ПОЛНОЙ ЗАДАЧИ -------\n";
    for (size_t i = 0; i < N; i += (N-1) / 10) {
        cout << "В точке x = " << x[i] << ": u(x) = " << u_com[i] << endl;
    }
    cout << "-------------\n";

}
